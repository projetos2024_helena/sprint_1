const express = require("express"); //importa o modulo express
const app = express(); // cria a instancia do app express 
const cors = require('cors'); // importa o modulo cors
const testConnect = require('./db/testConnect')


class AppController { // define a classe 
  constructor() {
    this.express = express();
    this.middlewares();
    this.routes();
    this.testConnect = testConnect();
  }

  middlewares() { // define middle
    this.express.use(express.json());
    this.express.use(cors())
  }
  routes() { // define as rotas 
    const apiRoutes= require('./routes/routes')
    this.express.use('/passagem/', apiRoutes)
    this.express.get("/teste/", (_, res) => {
      res.send({ status: "Api ativada." });
    });
  }
}

module.exports = new AppController().express;
