const router = require('express').Router();
const clienteController = require('../controller/clienteController');
const passagemController = require('../controller/passagemController');
const ReservaController = require('../controller/reservaController');
const VooController = require('../controller/vooController');
const dbController = require('../controller/dbController');


//cliente
router.post('/cliente/', clienteController.postCliente);
router.get('/Listacliente/', clienteController.getCliente);
router.put('/Atualizarcliente/', clienteController.updateCliente);
router.delete('/deleteCliente/', clienteController.deleteCliente);
router.post('/Logincliente/', clienteController.loginCliente);


//voo
router.post('/Voo', VooController.postVoo);
router.get('/listarVoo', VooController.getVoo);
router.put('/atualizarVoo/', VooController.updateVoo);
router.delete('/deleteVoo/', VooController.deletarVoo);


//passagem
router.post('/passagem/', passagemController.postPassagens);
router.get('/listapassagem', passagemController.getpassagens);
router.put('/updatepassagem', passagemController.updatepassagem);
router.delete('/deletepassagem', passagemController.deletepassagem);


//reserva
router.post('/criarReserva', ReservaController.postReserva);
router.get('/listarReservas', ReservaController.getReserva);
router.put('/atualizarReserva', ReservaController.updateReserva);
router.delete('/deletarReserva', ReservaController.deleteReserva);


// Rota para consultar todas as tabelas e suas descrições
router.get("/tabelas", dbController.getTable);


// Rota para consultar apenas os nomes das tabelas
router.get("/nomes", dbController.getTableNames);









module.exports = router;