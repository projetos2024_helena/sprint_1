const connect = require("../db/connect");

module.exports = class clienteController {

    static async postCliente(req, res) {
        const { nome, email, telefone, password } = req.body;
        if (nome === "" || email === "" || telefone === "" || password === "") {
          res.status(400).json({ message: "Preencha todos os campos obrigatórios." });
        } else {
          try {
            // lógica para verificar se o e-mail já está em uso
            res.status(201).json({ message: "Cliente cadastrado com sucesso." });
          } catch (error) {
            console.error('Erro ao cadastrar cliente:', error);
            res.status(500).json({ message: "Erro ao cadastrar cliente." });
          }
        }
      }
  
    static async getCliente(req, res) {
      //para listar 
      const listaCliente = [
            {
            nome: "Gustavo",
            email: "gugmaringolo@gmail.com",
            telefone: "16991133716",
            password: "Guga2020!?",
            },
      ];
    
      // verifica se os campos estão vazios 
      listaCliente.forEach(cliente => {
        if (cliente.nome === "" || cliente.email === "" || cliente.telefone === "" || cliente.password === "") {
          res.status(500).json({ message: "Erro ao buscar lista de clientes." });
          return; 
        }
      });
    
      res.status(200).json({ cliente: listaCliente });
    }
    
    // atualização de Cliente
    static async updateCliente(req, res) {
      const { nome, email, telefone, password } = req.body;
      if (nome === "" || email === "" || telefone === "" || password === "") {
        return res.status(200).json({ message: "Preencha os campos corretamente." });
      } else {
        return res.status(200).json({ message: "Cliente atualizado com sucesso." });
      }
    }
  
    // deleção de cliente
  static async deleteCliente(req, res) {
    const clienteId = req.body.id; // Obtendo o ID do cliente do corpo da requisição
    // Simulando um banco de dados de usuários
    let cliente = [
      { id: 1, nome: 'user1', email: 'email1', telefone: 'telefone1', password: '123' },
      { id: 2, nome: 'user2', email: 'email2', telefone: 'telefone2', password: '456' }
    ];
    // Encontrando o cliente pelo ID e removendo-o do array
    const index = cliente.findIndex(cliente => cliente.id === parseInt(clienteId));
    if (index !== -1) {
      cliente.splice(index, 1);
      // Enviando resposta de sucesso após a deleção do cliente
      res.status(200).json({ message: "Cliente removido com sucesso!" });
    } else {
      // Enviando resposta de cliente não encontrado
      res.status(404).json({ message: "Cliente não encontrado." });
    }
  }
  
  
   // login de Cliente
  static async loginCliente(req, res) {
    const { email, password } = req.body; // Incluído cpf na desestruturação
    // Simulando um banco de dados de cliente
    const cliente = [
      { nome: 'cliente1', email: 'email1', telefone: 'telefone1', password: '123' }
    ];
    const foundCliente = cliente.find(u => u.email === email && u.password === password);
    if (foundCliente) {
      res.status(200).json({ message: 'Login bem-sucedido!' });
    } else {
      res.status(401).json({ message: 'Email ou senha incorretos.' });
    }
  }
};


