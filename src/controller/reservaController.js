module.exports = class reservaController{

    static async postReserva(req, res) { // criação de uma nova reserva
        const {Cliente, AssentosReservados, ValorTotal, Numero_do_Voo } = req.body;
        // verifica se os campos estão vazios 
        if (Cliente === "" || AssentosReservados === "" || ValorTotal === "" || Numero_do_Voo === "" ) {
          res.status(500).json({ message: "Parece que alguns campos foram preenchidos incorretamente."});
        } else {
          res.status(200).json({ message: "Sua Reserva foi criada com sucesso." });
        }
      }

      static async getReserva(req, res) {
        //para fazer a listagem
        const listarReservas= [
          {
            Cliente:"Leo", 
            Assentos_Reservados:"78,79",
            ValorTotal:"5.600,00",
            Numero_do_Voo:"45678"
          },
        ];
    
        // verificando se não há campos vazios 
        listarReservas.forEach(reserva => {
          if (reserva.Cliente === "" ||  reserva.Assentos_Reservados === "" || reserva.ValorTotal === "" ) {
            res.status(500).json({ message: "Parece que ha um erro ao buscar a lista de Reservas." });
            return; 
          }
        });
      
        res.status(200).json({ Reserva: listarReservas });
      }

        // atualização das reservas
    static async updateReserva(req, res) {
        const {Cliente, Numero_do_Voo, AssentosReservados, ValorTotal} = req.body;
        if ( Cliente === "" || Numero_do_Voo === "" || AssentosReservados === "" || ValorTotal === "" ) {
          return res.status(500).json({ message: "infelizmente alguns campos foram preenchidos incorretamente." });
        } else {
          return res.status(200).json({ message: "Sua reserva foi atualizada com sucesso." });
        }
      }

       // deleção da reserva
  static async deleteReserva(req, res) {
    const reservaId = req.body.id; // Obtendo o ID da reserva do corpo da requisição
    // Simulando um banco de dados de reserva
    let reserva = [
      { id: 1, Cliente:"Leo", Assentos_Reservados:"78", ValorTotal:"5.600,00", Numero_do_Voo:"45679",},
      { id: 2, Cliente:"Gabi", Assentos_Reservados:"79", ValorTotal:"5.500,00", Numero_do_Voo:"45678", }
    ];
    // Encontrando a reserva pelo ID e removendo-o do array
    const index = reserva.findIndex(reserva => reserva.id === parseInt(reservaId));
    if (index !== -1) {
      reserva.splice(index, 1);
      // Enviando resposta de sucesso após a deleção da reserva
      res.status(200).json({ message: "Reserva excluida com exito!" });
    } else {
      // Enviando resposta de reserva não encontrada
      res.status(404).json({ message: "infelizmente não foi possivel encontrar a reserva." });
    }
  }     
}