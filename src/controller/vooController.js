module.exports = class vooController {
  static async postVoo(req, res) {
    // criação de uma nova passagem
    const {
      Numero_do_Voo,
      Origem,
      Destino,
      Horario_de_Partida,
      Horario_de_Chegada,
      Data,
    } = req.body;
    // verifica se os campos estão vazios
    if (
      Numero_do_Voo === "" ||
      Origem === "" ||
      Destino === "" ||
      Data === "" ||
      Horario_de_Partida === "" ||
      Horario_de_Chegada === ""
    ) {
      res
        .status(500)
        .json({ message: "alguns campos foram preenchidos incorretamente." });
    } else {
      res.status(200).json({ message: "Seu Voo foi criado com sucesso." });
    }
  }

  static async getVoo(req, res) {
    //para fazer a listagem
    const listarVoos = [
      {
        Numero_do_Voo: "VOO001",
        Origem: "Nova York",
        Destino: "Paris",
        Horario_de_Partida: "08:00",
        Horario_de_Chegada: "22:00",
        Data: "04/12/2023",
      },
    ];

    // verificando se não há campos vazios
    listarVoos.forEach((Voo) => {
      if (
        Voo.Numero_do_Voo === "" ||
        Voo.Origem === "" ||
        Voo.Destino === "" ||
        Voo.Data === "" ||
        Voo.Horario_de_Partida === "" ||
        Voo.Horario_de_Chegada === ""
      ) {
        res
          .status(500)
          .json({ message: "Há algum um erro ao buscar a lista de Voos." });
        return;
      }
    });

    res.status(200).json({ voos: listarVoos });
  }

  // atualização do voo
  static async updateVoo(req, res) {
    const {
      Numero_do_Voo,
      Origem,
      Destino,
      Horario_de_Partida,
      Horario_de_Chegada,
      Data,
    } = req.body;
    if (
      Numero_do_Voo === "" ||
      Origem === "" ||
      Destino === "" ||
      Data === "" ||
      Horario_de_Partida === "" ||
      Horario_de_Chegada === ""
    ) {
      return res
        .status(500)
        .json({
          message:
            "infelizmente alguns campos foram preenchidos incorretamente.",
        });
    } else {
      return res
        .status(200)
        .json({ message: "Seu Voo foi atualizado com sucesso." });
    }
  }

  // deleção de passagem
  static async deletarVoo(req, res) {
    const VooId = req.body.id; // Obtendo o ID do voo do corpo da requisição
    // Simulando um banco de dados de reserva
    let Voo = [
      {
        id: 1,
        Numero_do_Voo: "VOO001",
        Origem: "Nova York",
        Destino: "Paris",
        Horario_de_Partida: "08:00",
        Horario_de_Chegada: "22:00",
        Data: "04/12/2023",
      },
      {
        id: 2,
        Numero_do_Voo: "VOO0024",
        Origem: "Paris",
        Destino: "Nova York",
        Horario_de_Partida: "08:00",
        Horario_de_Chegada: "22:00",
        Data: "14/12/2023",
      },
    ];
    // Encontrando o voo pelo ID e removendo-o do array
    const index = Voo.findIndex((Voo) => Voo.id === parseInt(VooId));
    if (index !== -1) {
      Voo.splice(index, 1);
      // Enviando resposta de sucesso após a deleção do voo
      res.status(200).json({ message: "Voo excluido com exito!" });
    } else {
      // Enviando resposta de voo não encontrado
      res
        .status(404)
        .json({
          message: "infelizmente não foi possivel encontrar o seu Voo.",
        });
    }
  }
};
