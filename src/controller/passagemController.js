module.exports = class passagemController {
  static async postPassagens(req, res) {
    // criação de uma nova passagem
    const {
      Origem,
      Destino,
      Data_e_Hora_de_Saida,
      Data_e_Hora_de_Chegada,
      Assentos,
      Numero_do_Voo,
      Companhia_Aerea,
      Classe,
      Preço,
    } = req.body;
    // verifica se os campos estão vazios
    if (
      Origem === "" ||
      Destino === "" ||
      Data_e_Hora_de_Saida === "" ||
      Data_e_Hora_de_Chegada === "" ||
      Assentos === "" ||
      Numero_do_Voo === "" ||
      Companhia_Aerea === "" ||
      Classe === "" ||
      Preço === ""
    ) {
      res
        .status(500)
        .json({
          message: "Parece que alguns campos foram preenchidos incorretamente.",
        });
    } else {
      res
        .status(200)
        .json({ message: "Sua passagem foi cadastrada com sucesso." });
    }
  }

  static async getpassagens(req, res) {
    //para fazer a listagem
    const listapassagem = [
      {
        Origem: "Mexico",

        Destino: "Italia",

        Data_e_Hora_de_Saida: "22/09/24 08:00",

        Data_e_Hora_de_Chegada: "22/09/24 20:00",

        Assentos: "65",

        Numero_do_Voo: "44567",

        Companhia_Aerea: "Neos Air",

        Classe: "executiva",

        Preço: "6.370",
      },
    ];

    // verificando se não há campos vazios
    listapassagem.forEach((pass) => {
      if (
        pass.Origem === "" ||
        pass.Destino === "" ||
        pass.Data_e_Hora_de_Saida === "" ||
        pass.Data_e_Hora_de_Chegada === "" ||
        pass.Assentos === "" ||
        pass.Numero_do_Voo === "" ||
        pass.Companhia_Aerea === "" ||
        pass.Classe === "" ||
        pass.Data_e_Hora_de_Saida === ""
      ) {
        res
          .status(500)
          .json({
            message: "Encontramos um erro ao buscar a lista de usuários.",
          });
        return;
      }
    });

    res.status(200).json({ Passagens: listapassagem });
  }

  // atualização das passagens
  static async updatepassagem(req, res) {
    const {
      Origem,
      Destino,
      Data_e_Hora_de_Saida,
      Data_e_Hora_de_Chegada,
      Assentos,
      Numero_do_Voo,
      Companhia_Aerea,
      Classe,
      Preço,
    } = req.body;
    if (
      Origem === "" ||
      Destino === "" ||
      Data_e_Hora_de_Saida === "" ||
      Data_e_Hora_de_Chegada === "" ||
      Assentos === "" ||
      Numero_do_Voo === "" ||
      Companhia_Aerea === "" ||
      Classe === "" ||
      Preço === ""
    ) {
      return res
        .status(500)
        .json({
          message: "Parece que alguns campos foram preenchidos incorretamente.",
        });
    } else {
      return res
        .status(200)
        .json({ message: "Sua passagem foi atualizada com sucesso." });
    }
  }

  // deleção de passagem
  static async deletepassagem(req, res) {
    const passId = req.body.id; // Obtendo o ID da passagem do corpo da requisição
    // Simulando um banco de dados de passagem
    let pass = [
      {
        id: 1,
        Origem: "Brasil",
        Destino: "Italia",
        Data_e_Hora_de_Saida: "22/09/24 08:00",
        Data_e_Hora_de_Chegada: "22/09/24 20:00",
        Assentos: "65",
        Numero_do_Voo: "44567",
        Companhia_Aerea: "Neos Air",
        Classe: "executiva",
        Preço: "6.370",
      },
      {
        id: 2,
        Origem: "Italia",
        Destino: "Brasil",
        Data_e_Hora_de_Saida: "04/10/24 08:00",
        Data_e_Hora_de_Chegada: "04/10/24 20:00",
        Assentos: "67",
        Numero_do_Voo: "44677",
        Companhia_Aerea: "Neos Air",
        Classe: "executiva",
        Preço: "6.370",
      },
    ];
    // Encontrando a passagem pelo ID e removendo-o do array
    const index = pass.findIndex((pass) => pass.id === parseInt(passId));
    if (index !== -1) {
      pass.splice(index, 1);
      // Enviando resposta de sucesso após a deleção da passagem
      res.status(200).json({ message: "Passagem excluida com exito!" });
    } else {
      // Enviando resposta de passagem não encontrada
      res
        .status(404)
        .json({
          message: "infelizmente não foi possivel encontrar a passagem.",
        });
    }
  }
};
