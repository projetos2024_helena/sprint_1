const connect = require('../db/connect');

module.exports = class dbController {
    static async getTable(req, res) {
        // Consulta para obter a lista de tabelas
        const queryShowTables = "SHOW TABLES";

        connect.query(queryShowTables, async function (err, result, fields) {
            if (err) {
                console.log(err);
                return res.status(500).json({ error: "Erro ao obter tabelas do banco dados" });
            }

            const nameTable = result.map(row => row[Object.keys(row)[0]]);
            console.log("Nomes das tabelas dos bancos de dados:", nameTable);
            const tableNames = result.map(row => row[fields[0].name]);
            console.log("Tabela do banco de dados:", tableNames);

            const tables = [];

            for (let i = 0; i < result.length; i++) {
                const tableName = result[i][`Tables_in_${connect.config.connectionConfig.database}`];
                const queryDescTable = `DESCRIBE ${tableName}`;

                try {
                    const tableDescription = await new Promise((resolve, reject) => {
                        connect.query(queryDescTable, function (err, result, fields) {
                            if (err) {
                                reject(err);
                            }
                            resolve(result);
                        });
                    });
                    tables.push({ name: tableName, description: tableDescription });
                } catch (error) {
                    console.log(error);
                    return res.status(500).json({ error: "Erro ao obter a descrição da tabela!" });
                }
            }

            res.status(200).json({ message: "Obtendo todas as tabelas e suas descrições", tables });
        });
    }

    static async getTableNames(req, res) {
        const queryShowTables = "SHOW TABLES";

        connect.query(queryShowTables, function (err, result, fields) {
            if (err) {
                console.log(err);
                return res.status(500).json({ error: "Erro ao obter tabelas do banco dados" });
            }

            const tableNames = result.map(row => row[fields[0].name]);
            res.status(200).json({ tableNames });
        });
    }
}

